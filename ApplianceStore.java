import java.util.Scanner;

public class ApplianceStore{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		WashingMachine[] machines = new WashingMachine[4];
		for(int i = 0; i < machines.length; i++){
			machines[i] = new WashingMachine();
			System.out.println("Enter the brand:");
			machines[i].brand = reader.next();
			System.out.println("How much energy does it consume?");
			machines[i].energyUsage = reader.nextInt();
			System.out.println("Enter the price:");
			machines[i].price = reader.nextDouble();
			System.out.println("Is it smart? If yes, enter [true]. If not, enter [false].");
			machines[i].isSmart = reader.nextBoolean();
			System.out.println("Does it load from the front or the top?");
			machines[i].loadType = reader.next();
		}
		System.out.println(machines[3].brand);
		System.out.println(machines[3].energyUsage);
		System.out.println(machines[3].price);
		System.out.println(machines[3].isSmart);
		System.out.println(machines[3].loadType);
		machines[0].displayInformation();
		machines[0].promote();
	}
}