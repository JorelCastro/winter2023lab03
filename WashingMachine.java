public class WashingMachine{
	public String brand;
	public int energyUsage;
	public double price;
	public boolean isSmart;
	public String loadType;
	
	public void displayInformation(){
		System.out.println(brand + " " + loadType + " Load Washing Machine");
		System.out.println("$" + price + " + tax");
		if(isSmart){
			System.out.println("This washing machine includes smart assistant features.");
		}
		if(energyUsage < 500){
			System.out.println("ENERGY STAR® Certified");
		}
	}
	
	public void promote(){
		if(price < 750){
			System.out.println("This " + brand + " washing machine is at an affordable price.");
		}else if(price > 1400){
			System.out.println("This model might be too pricey. You can check other models.");
		}
		if(energyUsage < 500){
			System.out.println("This washing machine is energy efficient, which protects the planet and saves you money.");
		}
		if(isSmart){
			System.out.println("You can use your smartphone to control this machine.");
		}
	}
}